# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

automate permettant d'automatiser les tests du logiciel SBP

### How do I get set up? ###

les configurations des tests sont á placer dans des fichiers txt dans le dossier **configFiles**
Les configurations á tester sont á insérer dans le fichier **mainConfig.txt**
Pour faire fonctioner l'automatisation, un fichier **envConfig.txt** doit être créé et placé á la racine du projet
La structure de ce fichier doit être sous cette forme:
AUTOMA_PATH = " PATH_TO_AUTOMA.exe " (garder les espaces entre les doubles quotes!)
SBP_ROOT = "PATH_TO_SBP_BIN_FOLDER"
SBP_NAME = "SBP_EXE_NAME.exe"
SBP_PATH = "COMPLETE_PATH_TO_SBP_EXE.exe"

Cet automate gère les languages d'OS suivants: fr_FR, en_GB, en_US