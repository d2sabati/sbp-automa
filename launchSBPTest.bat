@echo off
IF EXIST %userprofile%\desktop\sbp-automa-tests GOTO START_APP
mkdir %userprofile%\desktop\sbp-automa-tests
:START_APP
ECHO Folder path : %~dp0
set /p PATH_AUTOMA=<%~dp0/automaConfig.txt
ECHO Automa path given : %PATH_AUTOMA%
ECHO If not correct, please change it in automaConfig.txt
set /p input = Press enter to continue
IF EXIST %PATH_AUTOMA% (
	IF EXIST %~dp0/testAutoma.py start %PATH_AUTOMA% %~dp0/testAutoma.py
) ELSE (
	ECHO your automa path is incorrect, please be sure to check your path in envConfig.txt
	set /p input = Press enter to exit
	exit
)
