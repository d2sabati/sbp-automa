#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from automa.api import *
import os, errno, shutil, datetime, locale, getpass, ctypes, numpy as np

#variables systeme
USER = getpass.getuser()
COMPUTERNAME = os.environ['COMPUTERNAME']
windll = ctypes.windll.kernel32
OS_LANG = locale.windows_locale[ windll.GetUserDefaultUILanguage() ]
print(OS_LANG)

#GERE LA LANGUE
SHORTCUTS = {}
SHORTCUTS['ALT_A'] = {'fr_FR':ALT+'a',
					'en_GB':ALT+'d',
					'en_US':ALT+'d'}
SHORTCUTS['ALT_N'] = {'fr_FR':ALT+'n',
					'en_GB':ALT+'n',
					'en_US':ALT+'n'}

TEXT = {}
TEXT['SELECT_FOLDER'] = {'fr_FR':'Sélectionner un dossier',
					'en_GB':'Select folder',
					'en_US':'Select folder'}
TEXT['OPEN_FILE'] = {'fr_FR':'Ouvrir',
					'en_GB':'Open',
					'en_US':'Open'}
					
#VARIABLES DE BASES
DIR_PATH = os.getcwd()
DATA_DIR = "sbp-validation-data/"
DESKTOP_PATH = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
PROJECTS_DIR = DESKTOP_PATH + "/sbp-automa-tests"
print("repertoire des projets créés: " + PROJECTS_DIR)
APP_NAME = "SB_Process"

COMMIT = "12345"
SBP_ROOT = ""
TITRE = ""
SBP_NAME = ""
DATA_PATH = ""
LINES_PATH = ""
LINES_NAME = ""
PP_PATH = ""
PP_NAME = ""
SOUNDING_REDUCTION = ""
METADATA_NAME = ""
PROJECT_NAME = ""
PROJECT_PATH = ""
TEST_FUNC = ""

def getDate(secondes):
	now=datetime.datetime.now()
	timestamp = 0;
	if secondes == 0:
		timestamp=now.strftime("%Y-%m-%d_%H-%M")
	else:
		timestamp=now.strftime("%Y-%m-%d_%H-%M-%S")
	return(timestamp)

#clone un répertoire
def cloneDir(src,dst):
	try:
		global file
		#si le path existe deja, on le supprime avant de le copier
		if os.path.exists(dst):
			shutil.rmtree(dst)
		#on copie les dossiers
		shutil.copytree(src, dst)
	except OSError as e:
		# si une exception est levé car le test n'est pas un répertoire
		if e.errno == errno.ENOTDIR:
			#on copie les fichiers
		   shutil.copy(src, dst)
		else:
			print('Directory not copied. Error: %s' % e)

#recupere les données du fichier de config
def getConfigDatas(file_path):
    vessel = {}
    execfile(file_path, vessel)
    return(vessel)
	
#permet de recuperer une liste de mot
def break_words(toSplit):
	#This function will split words
    words = toSplit.split(" ")
    return words
	
#liste des fonctionalitées
def createProject():
	timestamp = getDate(0)
	press(CTRL+"n")
	PROJECT_NAME = "SBP_Project_"+timestamp
	#création d'un nouveau projet
	write(PROJECT_NAME, into = "Project Name :")
	click("Browse")
	press(SHORTCUTS['ALT_A'][OS_LANG])
	write(PROJECTS_DIR)
	press(ENTER)
	click(TEXT['SELECT_FOLDER'][OS_LANG])
	click(ComboBox(to_right_of="Sounding Reduction :"))
	click(ListItem(SOUNDING_REDUCTION))
	click("Select")
	press(SHORTCUTS['ALT_A'][OS_LANG])
	write(DATA_PATH)
	press(ENTER)
	press(SHORTCUTS['ALT_N'][OS_LANG])
	write(METADATA_NAME)
	click(TEXT['OPEN_FILE'][OS_LANG])
	click("Create")

def openProject():
	PROJECT_COPY_PATH = PROJECTS_DIR + "/" + PROJECT_NAME
	print("repertoire de copie: " + PROJECT_COPY_PATH)
	cloneDir(PROJECT_PATH.replace('\\','/'), PROJECT_COPY_PATH)
	global PROJECT_PATH
	PROJECT_PATH = PROJECT_COPY_PATH
	press(CTRL+"o")
	press(SHORTCUTS['ALT_A'][OS_LANG])
	write(PROJECT_PATH)
	press(ENTER)
	click(TEXT['SELECT_FOLDER'][OS_LANG])

def closeProject():
	press(CTRL+"w")
	PROJECT_CREATED = 0
	
def importLines():
	#importation des lignes
	press(CTRL+"l")
	press(SHORTCUTS['ALT_A'][OS_LANG])
	write(LINES_PATH)
	press(ENTER)
	press(SHORTCUTS['ALT_N'][OS_LANG])
	write(LINES_NAME)
	click(TEXT['OPEN_FILE'][OS_LANG])

def importPP():
	#importation des lignes
	press(CTRL+"p")
	press(SHORTCUTS['ALT_A'][OS_LANG])
	write(PP_PATH)
	press(ENTER)
	press(SHORTCUTS['ALT_N'][OS_LANG])
	write(PP_NAME)
	click(TEXT['OPEN_FILE'][OS_LANG])

def loadPP():
	click(CheckBox(to_left_of="All"))
	press(CTRL+SHIFT+"p")
	click(ComboBox())
	click(ListItem(PP_NAME.split('.', 1)[0]))
	click("Select")
	click(CheckBox(below="PP"))
	click(CheckBox(to_left_of="All"))

def setSurveyParameters():
	press(SHIFT+"s")
	click("Sounding Reduction")
	click(ComboBox(to_right_of="Type"))
	click(ListItem(SOUNDING_REDUCTION))
	click(Button("Save"))

def georef():
	#selection des lignes
	click(CheckBox(to_left_of="All"))
	press(CTRL+"g")
	click("OK Enter")
	click(CheckBox(to_left_of="All"))

#on associe une clé aux fonctions créées
ALL_FUNC = {'createProject':createProject,
			'openProject':openProject,
			'closeProject':closeProject,
			'importLines':importLines,
			'setSurveyParam':setSurveyParameters,
			'importPP':importPP,
			'loadPP':loadPP,
			'georef':georef}

#on récupére les informations sur la configuration de tests
masterConfig = getConfigDatas(os.path.join(DIR_PATH, 'mainConfig.txt'))
envConfig = getConfigDatas(os.path.join(DIR_PATH, 'envConfig.txt'))
SBP_ROOT = envConfig['SBP_ROOT']
SBP_NAME = envConfig['SBP_NAME']
ALL_TESTS_CONFIG = break_words(masterConfig['configToTest'])
SBP_EXEC=SBP_ROOT+SBP_NAME
	
#on récupere les informations des configurations pour chaques fichiers config
for conf in ALL_TESTS_CONFIG:
	#on ouvre en écriture le fichier d'output
	outputLog = open(PROJECTS_DIR+"/"+getDate(0)+".md","w")
	outputLog.write("---\n")
	SUCCESS = 1
	try:
		CONFIG_DATA_PATH = os.path.join(DIR_PATH, 'configFiles/'+conf)
		config = getConfigDatas(CONFIG_DATA_PATH)
		DATA_PATH = os.path.join(DIR_PATH,DATA_DIR+config['pathToDataset'])
		TITRE = config['titre']
		METADATA_NAME = config['metadataName']
		LINES_PATH = os.path.join(DATA_PATH,config['pathToLines'])
		LINES_NAME = config['nameLines']
		PP_PATH = os.path.join(DATA_PATH,config['pathToPostProcs'])
		PP_NAME = config['namePostProcs']
		if config['soundingReduction'] == "ERS" :
			SOUNDING_REDUCTION = "Ellipsoidal"
		else :
			SOUNDING_REDUCTION = "Water Level"
		PROJECT_NAME = config['projectName']
		PROJECT_PATH = DESKTOP_PATH + config['projectPath']
		TEST_FUNC = break_words(config['tests'])
		
		outputLog.write("uuid: " + getDate(0) + "\n")
		outputLog.write("titre: " + TITRE + "\n")
		outputLog.write("utilisateur: " + USER + "\n")
		outputLog.write("machine: " + COMPUTERNAME + "\n")
		outputLog.write("commit: " + COMMIT + "\n")
	except:
		print(getDate(1) + " : " + conf + " isn't a known/correct config file, session is now terminating\n")
		break

	print("path des lignes: " + LINES_PATH)
	print("path de l'application: " + SBP_EXEC)

	start(SBP_EXEC)
	outputLog.write("test:\n")
	
	#on lance les tests 
	for func in TEST_FUNC:
		outputLog.write("  - nom: " + func + "\n")
		try:
			ALL_FUNC[func]()
			outputLog.write("    result: passed\n")
		except:
			SUCCESS = 0
			outputLog.write("    result: failed\n")
			#break
	outputLog.write("---\n")	
	kill(APP_NAME)
	outputLog.close()